#include "Player.h"
#include "AssetManager.h"

Player::Player(sf::Vector2u screenSize) 
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerSprite.png"), 160, 160, 8.0f)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, previousPosition()
	, collide(false)
{
	// label each fram of the player sprite sheet
	AddClip("Down", 0, 0);
	AddClip("Right", 1, 1);
	AddClip("Up", 2, 2);
	AddClip("Left", 3, 3);

	// Set starting frame
	PlayClip("Up", false);
	
	// position the player at the centre of the screen	
	spawnPoint.x = (((float)screenSize.x - sprite.getGlobalBounds().width) / 2.0f);
	spawnPoint.y = ((float)screenSize.y - sprite.getGlobalBounds().height) / 2.0f;
	sprite.setPosition(spawnPoint);
}

bool Player::GetCollide()
{
	return collide;
}

void Player::Input()
{
	// set velocity to be 0 while no input is detected
	velocity.x = 0;
	velocity.y = 0;

	// Move up
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) 
		|| sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		velocity.y = -speed;
		PlayClip("Up", false);
	}

	// Move down
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
		|| sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		velocity.y = speed;
		PlayClip("Down", false);
	}

	// Move left
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
		|| sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		velocity.x = -speed;
		PlayClip("Left", false);
	}

	// Move right
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)
		|| sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		velocity.x = speed;
		PlayClip("Right", false);
	}
}

void Player::Update(sf::Time frameTime)
{
	// Calculate the new velocity
	// Save old position
	previousPosition = sprite.getPosition();

	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

	// Move the player to the new position
	sprite.setPosition(newPosition);
	
	// Animate the sprite
	AnimatingObject::Update(frameTime);
}

void Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	if (GetHitBox().intersects(otherHitbox))
	{
		collide = true;
	}
	else
	{
		collide = false;
	}
}