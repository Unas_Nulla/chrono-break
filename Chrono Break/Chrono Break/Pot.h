#pragma once
#include "PuzzleObject.h"

class Pot :
    public PuzzleObject
{
public:

    // Constructor
    Pot(bool isPresent, bool hasLight, bool hasPlant, bool hasFruit);

    void Update(sf::Time frameTime);

    bool isPresent;
    bool hasLight;
    bool hasPlant;
    bool hasFruit;

private:
     
    
};