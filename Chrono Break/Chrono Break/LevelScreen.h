#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "PuzzleObject.h"
#include "AssetManager.h"
#include "Pot.h"
#include "TimeRift.h"

class Game;

class LevelScreen
{
public:
	LevelScreen(Game* newGamePointer);

	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);

private:
	Game* gamePointer;
	Player playerInstance;
	std::vector<PuzzleObject*> objects;
	std::vector<Pot*> pots;
	Pot currentPot;
	TimeRift timeRift;
	sf::View camera;
	sf::Font gameFont;
	sf::Text testText;
	sf::Text debug;
};