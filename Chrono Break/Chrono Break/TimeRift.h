#pragma once
#include "PuzzleObject.h"
class TimeRift :
    public PuzzleObject
{
public:
    //Constructor
    TimeRift(sf::Vector2u screenSize);

    void Update(sf::Time frameTime);

private:

    sf::Vector2f position;
};

