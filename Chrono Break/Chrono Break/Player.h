#pragma once
#include "AnimatingObject.h"
#include "PuzzleObject.h"

class Player : public AnimatingObject
{
public:
	// Constructors / Destructors
	Player(sf::Vector2u screenSize);

	bool GetCollide();

	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	void HandleSolidCollision(sf::FloatRect otherHitbox);



private:
	// Data
	std::vector<PuzzleObject*> inventory;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2f spawnPoint;
	bool collide;
	sf::Vector2f previousPosition;
};

