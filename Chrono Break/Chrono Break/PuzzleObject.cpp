#include "PuzzleObject.h"
#include "AssetManager.h"

PuzzleObject::PuzzleObject(): AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/Key.png"), 96, 96, 1.0f)
		, visible(true)
{

}

void PuzzleObject::Update(sf::Time frameTime)
{
	sf::Vector2f originalPosition = sprite.getPosition();

	sf::Vector2f newPosition = sprite.getPosition();

	sprite.setPosition(newPosition);

	AnimatingObject::Update(frameTime);
}

void PuzzleObject::SetPosition(sf::Vector2f newPosition)
{
	sprite.setPosition(newPosition);
}