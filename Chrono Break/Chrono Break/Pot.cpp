#include "Pot.h"
#include "AssetManager.h"

Pot::Pot(bool isPresent, bool hasLight, bool hasPlant, bool hasFruit)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/Plant All.png"), 96, 96, 8.0f)
	, isPresent(isPresent)
	, hasLight(hasLight)
	, hasPlant(hasPlant)
	, hasFruit(hasFruit)
{
	// Label each frame
	AddClip("Pot", 0, 0);
	AddClip("LightPot", 1, 1);
	AddClip("Sapling", 2, 2);
	AddClip("Tree", 3, 3);
	AddClip("LemonTree", 4, 4);
	AddClip("Dead", 5, 5);

}

void Pot::Update(sf::Time frameTime)
{
	// Determine what frame to display
	if (!isPresent && hasPlant)
	{
		PlayClip("Sapling", false);
	}
	else if (isPresent && hasLight && hasPlant && !hasFruit)
	{
		PlayClip("Tree", false);
	}
	else if (isPresent && hasLight && hasPlant && hasFruit)
	{
		PlayClip("LemonTree", false);
	}
	else if (isPresent && !hasLight && hasPlant)
	{
		PlayClip("Dead", false);
	}
	else if (isPresent && hasLight && !hasPlant)
	{
		PlayClip("LightPot", false);
	}
	else
	{
		PlayClip("Pot", false);
	}

	// Animate the Pot
	PuzzleObject::Update(frameTime);
}