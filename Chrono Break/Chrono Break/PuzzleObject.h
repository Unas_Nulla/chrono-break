#pragma once
#include "AnimatingObject.h"
class PuzzleObject :
    virtual public AnimatingObject
{
public:
    // Constructors
    PuzzleObject();

    void Update(sf::Time frameTime);
    void SetPosition(sf::Vector2f newPosition);


private:
    bool visible;
};