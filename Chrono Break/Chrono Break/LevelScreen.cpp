#include "LevelScreen.h"
#include "Game.h"
#include <iostream>
#include <stdlib.h>

LevelScreen::LevelScreen(Game* newGamePointer)
	: playerInstance(newGamePointer->GetWindow().getSize())
	, gamePointer(newGamePointer)
	, camera(newGamePointer->GetWindow().getDefaultView())
	, gameFont(AssetManager::RequestFont("Assets/Fonts/enter-command.ttf"))
	, timeRift(newGamePointer->GetWindow().getSize())
	, currentPot(true, false, false, false)
{
	PuzzleObject* testObject = new PuzzleObject();

	// Calculate center of the screen
	sf::Vector2f newPosition;
	newPosition.x = newGamePointer->GetWindow().getSize().x / 2;
	newPosition.y = newGamePointer->GetWindow().getSize().y / 2;

	newPosition.x -= testObject->GetHitBox().width / 2;
	newPosition.y -= testObject->GetHitBox().height/ 2;

	testObject->SetPosition(newPosition);

	objects.push_back(testObject);	

	// Initialise plant pots
	Pot* pot1 = new Pot(true, false, false, false);
	Pot* pot2 = new Pot(true, true, false, false);
	Pot* pot3 = new Pot(true, false, false, false);
	Pot* pot4 = new Pot(true, false, true, false);
	
	// set plant pot positions
	pot1->SetPosition(sf::Vector2f(100, 300));
	pots.push_back(pot1);
	pot2->SetPosition(sf::Vector2f(300, 100));
	pots.push_back(pot2);
	pot3->SetPosition(sf::Vector2f(500, 200));
	pots.push_back(pot3);
	pot4->SetPosition(sf::Vector2f(900, 800));
	pots.push_back(pot4);

	// Test font
	testText.setFont(gameFont);
	testText.setString("Test");
	testText.setCharacterSize(50);
	testText.setFillColor(sf::Color::White);
	testText.setPosition(camera.getCenter().x - 500, camera.getCenter().y - 500);
}

void LevelScreen::Input()
{
	playerInstance.Input();
}

void LevelScreen::Update(sf::Time frameTime)
{
	// Run player update
	playerInstance.Update(frameTime);

	// Run Time rift update
	timeRift.Update(frameTime);

	for (int i = 0; i < objects.size(); i++)
	{		
		// Run object update
		objects[i]->Update(frameTime);

		// Check for player collision with objects
		playerInstance.HandleSolidCollision(objects[i]->GetHitBox());
		if (playerInstance.GetCollide() == true)
		{

		}
	}

	// Run update for pots vector
	for (int i = 0; i < pots.size(); ++i)
	{
		pots[i]->Update(frameTime);
	}
}

void LevelScreen::DrawTo(sf::RenderTarget& target)
{
	// Update camera position
	sf::Vector2f currentViewCenter = camera.getCenter();

	// Set camera view
	target.setView(camera);

	// Draw Time rift to screen
	timeRift.DrawTo(target);

	// Draw puzzle objects to the screen
	for (int i = 0; i < objects.size(); i++)
	{
		objects[i]->DrawTo(target);
	}

	// Draw plant pots onto screen
	for (int i = 0; i < pots.size(); i++)
	{
		pots[i]->DrawTo(target);
	}

	// Draw Player to screen
	playerInstance.DrawTo(target);

	// Draw text to screen
	target.draw(testText);

	//Remove camera view
	target.setView(target.getDefaultView());
}