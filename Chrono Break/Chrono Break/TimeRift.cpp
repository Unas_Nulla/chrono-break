#include "TimeRift.h"
#include "AssetManager.h"

TimeRift::TimeRift(sf::Vector2u screenSize)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/TimeRift.png"), 192, 192, 12.0f)
{
	AddClip("Rift", 0, 15);

	PlayClip("Rift", true);

	// position the rift relative to the centre of the screen	
	position.x = (((float)screenSize.x - sprite.getGlobalBounds().width) / 2.0f) + 200;
	position.y = (((float)screenSize.y - sprite.getGlobalBounds().height) / 2.0f) - 200;
	sprite.setPosition(position);
}

void TimeRift::Update(sf::Time frameTime)
{
	// Animate the sprite
	AnimatingObject::Update(frameTime);
}
