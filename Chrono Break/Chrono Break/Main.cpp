#include <SFML/Graphics.hpp>
#include "Game.h"
#include <stdlib.h>
#include <time.h>

int main()
{
	sf::RenderWindow window(sf::VideoMode::getDesktopMode(), "Chrono Break", sf::Style::Titlebar | sf::Style::Close);

	Game gameInstance;

	gameInstance.RunGameLoop();
	
	return 0;
}